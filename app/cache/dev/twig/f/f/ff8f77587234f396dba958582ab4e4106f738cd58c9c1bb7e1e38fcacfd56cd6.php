<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_ff8f77587234f396dba958582ab4e4106f738cd58c9c1bb7e1e38fcacfd56cd6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("TwigBundle::layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "TwigBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_520c55fa37b279f1d61676b2cddef01d0583d4ea0278c30ab8826a2e00b9b6c1 = $this->env->getExtension("native_profiler");
        $__internal_520c55fa37b279f1d61676b2cddef01d0583d4ea0278c30ab8826a2e00b9b6c1->enter($__internal_520c55fa37b279f1d61676b2cddef01d0583d4ea0278c30ab8826a2e00b9b6c1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_520c55fa37b279f1d61676b2cddef01d0583d4ea0278c30ab8826a2e00b9b6c1->leave($__internal_520c55fa37b279f1d61676b2cddef01d0583d4ea0278c30ab8826a2e00b9b6c1_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_06edfdf52cc153e5edf24543f1a0a97f353fee5b3dd8ea58b886d1b150121f00 = $this->env->getExtension("native_profiler");
        $__internal_06edfdf52cc153e5edf24543f1a0a97f353fee5b3dd8ea58b886d1b150121f00->enter($__internal_06edfdf52cc153e5edf24543f1a0a97f353fee5b3dd8ea58b886d1b150121f00_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_06edfdf52cc153e5edf24543f1a0a97f353fee5b3dd8ea58b886d1b150121f00->leave($__internal_06edfdf52cc153e5edf24543f1a0a97f353fee5b3dd8ea58b886d1b150121f00_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_7324ed7550c9dd1ed7d7d698c9a1aa01c93b032c1f50ac5fea6319a565f962d6 = $this->env->getExtension("native_profiler");
        $__internal_7324ed7550c9dd1ed7d7d698c9a1aa01c93b032c1f50ac5fea6319a565f962d6->enter($__internal_7324ed7550c9dd1ed7d7d698c9a1aa01c93b032c1f50ac5fea6319a565f962d6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_7324ed7550c9dd1ed7d7d698c9a1aa01c93b032c1f50ac5fea6319a565f962d6->leave($__internal_7324ed7550c9dd1ed7d7d698c9a1aa01c93b032c1f50ac5fea6319a565f962d6_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_a974e62e82de76e930e714a40c58e7153bdc53015ef826ebaaeacb461e613315 = $this->env->getExtension("native_profiler");
        $__internal_a974e62e82de76e930e714a40c58e7153bdc53015ef826ebaaeacb461e613315->enter($__internal_a974e62e82de76e930e714a40c58e7153bdc53015ef826ebaaeacb461e613315_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("TwigBundle:Exception:exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 12)->display($context);
        
        $__internal_a974e62e82de76e930e714a40c58e7153bdc53015ef826ebaaeacb461e613315->leave($__internal_a974e62e82de76e930e714a40c58e7153bdc53015ef826ebaaeacb461e613315_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
