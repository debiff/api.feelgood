<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Diet
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\DietRepository")
 */
class Diet
{
    /*
     * FOREIGN FIELDS
    */

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="diet")
     * @ORM\JoinColumn(name="uid", referencedColumnName="uid")
     */
    protected $user;
    /**
     * @ORM\ManyToOne(targetEntity="Dish", inversedBy="diet")
     * @ORM\JoinColumn(name="did", referencedColumnName="did")
     */
    protected $dish;

    /*
     * FIELDS OF DIET TABLE
    */
    /**
     * @var integer
     *
     * @ORM\Column(name="dietid", type="string", length=36)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $dietid;

    /**
     * @var integer
     *
     * @ORM\Column(name="day", type="integer")
     */
    private $day;

    /**
     * @var integer
     *
     * @ORM\Column(name="meal", type="integer")
     */
    private $meal;

    /**
     * @var integer
     *
     * @ORM\Column(name="start", type="integer")
     */
    private $start;

    /**
     * @var integer
     *
     * @ORM\Column(name="end", type="integer")
     */
    private $end;

    /**
     * @var integer
     *
     * @ORM\Column(name="grams", type="integer")
     */
    private $grams;



    /**
     * Get dietid
     *
     * @return string 
     */
    public function getDietid()
    {
        return $this->dietid;
    }

    /**
     * Set day
     *
     * @param integer $day
     * @return Diet
     */
    public function setDay($day)
    {
        $this->day = $day;

        return $this;
    }

    /**
     * Get day
     *
     * @return integer 
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * Set meal
     *
     * @param integer $meal
     * @return Diet
     */
    public function setMeal($meal)
    {
        $this->meal = $meal;

        return $this;
    }

    /**
     * Get meal
     *
     * @return integer 
     */
    public function getMeal()
    {
        return $this->meal;
    }

    /**
     * Set start
     *
     * @param integer $start
     * @return Diet
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return integer 
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set end
     *
     * @param integer $end
     * @return Diet
     */
    public function setEnd($end)
    {
        $this->end = $end;

        return $this;
    }

    /**
     * Get end
     *
     * @return integer 
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Set grams
     *
     * @param integer $grams
     * @return Diet
     */
    public function setGrams($grams)
    {
        $this->grams = $grams;

        return $this;
    }

    /**
     * Get grams
     *
     * @return integer 
     */
    public function getGrams()
    {
        return $this->grams;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     * @return Diet
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set dish
     *
     * @param \AppBundle\Entity\Dish $dish
     * @return Diet
     */
    public function setDish(\AppBundle\Entity\Dish $dish = null)
    {
        $this->dish = $dish;

        return $this;
    }

    /**
     * Get dish
     *
     * @return \AppBundle\Entity\Dish 
     */
    public function getDish()
    {
        return $this->dish;
    }
}
