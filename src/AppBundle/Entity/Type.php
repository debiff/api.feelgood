<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Type
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\TypeRepository")
 */
class Type
{
    /*
     * FOREIGN FIELDS
    */

    /**
     * @ORM\OneToMany(targetEntity="Food", mappedBy="type")
     */
    protected $food;

    /*
     * FIELDS OF TYPE TABLE
    */


    /**
     * @var integer
     *
     * @ORM\Column(name="tid", type="string", length=36)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $tid;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;



    /**
     * Constructor
     */
    public function __construct()
    {
        $this->food = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get tid
     *
     * @return string 
     */
    public function getTid()
    {
        return $this->tid;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Type
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Add food
     *
     * @param \AppBundle\Entity\Food $food
     * @return Type
     */
    public function addFood(\AppBundle\Entity\Food $food)
    {
        $this->food[] = $food;

        return $this;
    }

    /**
     * Remove food
     *
     * @param \AppBundle\Entity\Food $food
     */
    public function removeFood(\AppBundle\Entity\Food $food)
    {
        $this->food->removeElement($food);
    }

    /**
     * Get food
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFood()
    {
        return $this->food;
    }
}
