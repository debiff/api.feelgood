<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Food
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\FoodRepository")
 */
class Food
{
    /*
     * FOREIGN FIELDS
    */

    /**
     * @ORM\OneToMany(targetEntity="Allergy", mappedBy="food")
     */
    protected $allergy;
    /**
     * @ORM\OneToMany(targetEntity="Dish", mappedBy="food_main")
     */
    protected $dish_main;
    /**
     * @ORM\OneToMany(targetEntity="Dish", mappedBy="food_outline1")
     */
    protected $dish_outline1;
    /**
     * @ORM\OneToMany(targetEntity="Dish", mappedBy="food_outline2")
     */
    protected $dish_outline2;
    /**
     * @ORM\OneToMany(targetEntity="Dish", mappedBy="food_outline3")
     */
    protected $dish_outline3;
    /**
     * @ORM\OneToMany(targetEntity="Dish", mappedBy="food_outline4")
     */
    protected $dish_outline4;
    /**
     * @ORM\OneToMany(targetEntity="Dish", mappedBy="food_outline5")
     */
    protected $dish_outline5;
    /**
     * @ORM\OneToMany(targetEntity="Dish", mappedBy="food_outline6")
     */
    protected $dish_outline6;
    /**
     * @ORM\OneToMany(targetEntity="Dish", mappedBy="food_outline7")
     */
    protected $dish_outline7;
    /**
     * @ORM\OneToMany(targetEntity="Dish", mappedBy="food_outline8")
     */
    protected $dish_outline8;
    /**
     * @ORM\OneToMany(targetEntity="Dish", mappedBy="food_outline9")
     */
    protected $dish_outline9;
    /**
     * @ORM\ManyToOne(targetEntity="Type", inversedBy="food")
     * @ORM\JoinColumn(name="tid", referencedColumnName="tid")
     */
    protected $type;

    /*
     * FIELDS OF FOOD TABLE
    */


    /**
     * @var integer
     *
     * @ORM\Column(name="fid", type="string", length=36)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $fid;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="protein", type="integer")
     */
    private $protein;

    /**
     * @var integer
     *
     * @ORM\Column(name="carbohydrates", type="integer")
     */
    private $carbohydrates;

    /**
     * @var integer
     *
     * @ORM\Column(name="fat", type="integer")
     */
    private $fat;



    /**
     * Constructor
     */
    public function __construct()
    {
        $this->allergy = new \Doctrine\Common\Collections\ArrayCollection();
        $this->dish_main = new \Doctrine\Common\Collections\ArrayCollection();
        $this->dish_outline1 = new \Doctrine\Common\Collections\ArrayCollection();
        $this->dish_outline2 = new \Doctrine\Common\Collections\ArrayCollection();
        $this->dish_outline3 = new \Doctrine\Common\Collections\ArrayCollection();
        $this->dish_outline4 = new \Doctrine\Common\Collections\ArrayCollection();
        $this->dish_outline5 = new \Doctrine\Common\Collections\ArrayCollection();
        $this->dish_outline6 = new \Doctrine\Common\Collections\ArrayCollection();
        $this->dish_outline7 = new \Doctrine\Common\Collections\ArrayCollection();
        $this->dish_outline8 = new \Doctrine\Common\Collections\ArrayCollection();
        $this->dish_outline9 = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get fid
     *
     * @return string 
     */
    public function getFid()
    {
        return $this->fid;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Food
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set protein
     *
     * @param integer $protein
     * @return Food
     */
    public function setProtein($protein)
    {
        $this->protein = $protein;

        return $this;
    }

    /**
     * Get protein
     *
     * @return integer 
     */
    public function getProtein()
    {
        return $this->protein;
    }

    /**
     * Set carbohydrates
     *
     * @param integer $carbohydrates
     * @return Food
     */
    public function setCarbohydrates($carbohydrates)
    {
        $this->carbohydrates = $carbohydrates;

        return $this;
    }

    /**
     * Get carbohydrates
     *
     * @return integer 
     */
    public function getCarbohydrates()
    {
        return $this->carbohydrates;
    }

    /**
     * Set fat
     *
     * @param integer $fat
     * @return Food
     */
    public function setFat($fat)
    {
        $this->fat = $fat;

        return $this;
    }

    /**
     * Get fat
     *
     * @return integer 
     */
    public function getFat()
    {
        return $this->fat;
    }

    /**
     * Add allergy
     *
     * @param \AppBundle\Entity\Allergy $allergy
     * @return Food
     */
    public function addAllergy(\AppBundle\Entity\Allergy $allergy)
    {
        $this->allergy[] = $allergy;

        return $this;
    }

    /**
     * Remove allergy
     *
     * @param \AppBundle\Entity\Allergy $allergy
     */
    public function removeAllergy(\AppBundle\Entity\Allergy $allergy)
    {
        $this->allergy->removeElement($allergy);
    }

    /**
     * Get allergy
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAllergy()
    {
        return $this->allergy;
    }

    /**
     * Add dish_main
     *
     * @param \AppBundle\Entity\Dish $dishMain
     * @return Food
     */
    public function addDishMain(\AppBundle\Entity\Dish $dishMain)
    {
        $this->dish_main[] = $dishMain;

        return $this;
    }

    /**
     * Remove dish_main
     *
     * @param \AppBundle\Entity\Dish $dishMain
     */
    public function removeDishMain(\AppBundle\Entity\Dish $dishMain)
    {
        $this->dish_main->removeElement($dishMain);
    }

    /**
     * Get dish_main
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDishMain()
    {
        return $this->dish_main;
    }

    /**
     * Add dish_outline1
     *
     * @param \AppBundle\Entity\Dish $dishOutline1
     * @return Food
     */
    public function addDishOutline1(\AppBundle\Entity\Dish $dishOutline1)
    {
        $this->dish_outline1[] = $dishOutline1;

        return $this;
    }

    /**
     * Remove dish_outline1
     *
     * @param \AppBundle\Entity\Dish $dishOutline1
     */
    public function removeDishOutline1(\AppBundle\Entity\Dish $dishOutline1)
    {
        $this->dish_outline1->removeElement($dishOutline1);
    }

    /**
     * Get dish_outline1
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDishOutline1()
    {
        return $this->dish_outline1;
    }

    /**
     * Add dish_outline2
     *
     * @param \AppBundle\Entity\Dish $dishOutline2
     * @return Food
     */
    public function addDishOutline2(\AppBundle\Entity\Dish $dishOutline2)
    {
        $this->dish_outline2[] = $dishOutline2;

        return $this;
    }

    /**
     * Remove dish_outline2
     *
     * @param \AppBundle\Entity\Dish $dishOutline2
     */
    public function removeDishOutline2(\AppBundle\Entity\Dish $dishOutline2)
    {
        $this->dish_outline2->removeElement($dishOutline2);
    }

    /**
     * Get dish_outline2
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDishOutline2()
    {
        return $this->dish_outline2;
    }

    /**
     * Add dish_outline3
     *
     * @param \AppBundle\Entity\Dish $dishOutline3
     * @return Food
     */
    public function addDishOutline3(\AppBundle\Entity\Dish $dishOutline3)
    {
        $this->dish_outline3[] = $dishOutline3;

        return $this;
    }

    /**
     * Remove dish_outline3
     *
     * @param \AppBundle\Entity\Dish $dishOutline3
     */
    public function removeDishOutline3(\AppBundle\Entity\Dish $dishOutline3)
    {
        $this->dish_outline3->removeElement($dishOutline3);
    }

    /**
     * Get dish_outline3
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDishOutline3()
    {
        return $this->dish_outline3;
    }

    /**
     * Add dish_outline4
     *
     * @param \AppBundle\Entity\Dish $dishOutline4
     * @return Food
     */
    public function addDishOutline4(\AppBundle\Entity\Dish $dishOutline4)
    {
        $this->dish_outline4[] = $dishOutline4;

        return $this;
    }

    /**
     * Remove dish_outline4
     *
     * @param \AppBundle\Entity\Dish $dishOutline4
     */
    public function removeDishOutline4(\AppBundle\Entity\Dish $dishOutline4)
    {
        $this->dish_outline4->removeElement($dishOutline4);
    }

    /**
     * Get dish_outline4
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDishOutline4()
    {
        return $this->dish_outline4;
    }

    /**
     * Add dish_outline5
     *
     * @param \AppBundle\Entity\Dish $dishOutline5
     * @return Food
     */
    public function addDishOutline5(\AppBundle\Entity\Dish $dishOutline5)
    {
        $this->dish_outline5[] = $dishOutline5;

        return $this;
    }

    /**
     * Remove dish_outline5
     *
     * @param \AppBundle\Entity\Dish $dishOutline5
     */
    public function removeDishOutline5(\AppBundle\Entity\Dish $dishOutline5)
    {
        $this->dish_outline5->removeElement($dishOutline5);
    }

    /**
     * Get dish_outline5
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDishOutline5()
    {
        return $this->dish_outline5;
    }

    /**
     * Add dish_outline6
     *
     * @param \AppBundle\Entity\Dish $dishOutline6
     * @return Food
     */
    public function addDishOutline6(\AppBundle\Entity\Dish $dishOutline6)
    {
        $this->dish_outline6[] = $dishOutline6;

        return $this;
    }

    /**
     * Remove dish_outline6
     *
     * @param \AppBundle\Entity\Dish $dishOutline6
     */
    public function removeDishOutline6(\AppBundle\Entity\Dish $dishOutline6)
    {
        $this->dish_outline6->removeElement($dishOutline6);
    }

    /**
     * Get dish_outline6
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDishOutline6()
    {
        return $this->dish_outline6;
    }

    /**
     * Add dish_outline7
     *
     * @param \AppBundle\Entity\Dish $dishOutline7
     * @return Food
     */
    public function addDishOutline7(\AppBundle\Entity\Dish $dishOutline7)
    {
        $this->dish_outline7[] = $dishOutline7;

        return $this;
    }

    /**
     * Remove dish_outline7
     *
     * @param \AppBundle\Entity\Dish $dishOutline7
     */
    public function removeDishOutline7(\AppBundle\Entity\Dish $dishOutline7)
    {
        $this->dish_outline7->removeElement($dishOutline7);
    }

    /**
     * Get dish_outline7
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDishOutline7()
    {
        return $this->dish_outline7;
    }

    /**
     * Add dish_outline8
     *
     * @param \AppBundle\Entity\Dish $dishOutline8
     * @return Food
     */
    public function addDishOutline8(\AppBundle\Entity\Dish $dishOutline8)
    {
        $this->dish_outline8[] = $dishOutline8;

        return $this;
    }

    /**
     * Remove dish_outline8
     *
     * @param \AppBundle\Entity\Dish $dishOutline8
     */
    public function removeDishOutline8(\AppBundle\Entity\Dish $dishOutline8)
    {
        $this->dish_outline8->removeElement($dishOutline8);
    }

    /**
     * Get dish_outline8
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDishOutline8()
    {
        return $this->dish_outline8;
    }

    /**
     * Add dish_outline9
     *
     * @param \AppBundle\Entity\Dish $dishOutline9
     * @return Food
     */
    public function addDishOutline9(\AppBundle\Entity\Dish $dishOutline9)
    {
        $this->dish_outline9[] = $dishOutline9;

        return $this;
    }

    /**
     * Remove dish_outline9
     *
     * @param \AppBundle\Entity\Dish $dishOutline9
     */
    public function removeDishOutline9(\AppBundle\Entity\Dish $dishOutline9)
    {
        $this->dish_outline9->removeElement($dishOutline9);
    }

    /**
     * Get dish_outline9
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDishOutline9()
    {
        return $this->dish_outline9;
    }

    /**
     * Set type
     *
     * @param \AppBundle\Entity\Type $type
     * @return Food
     */
    public function setType(\AppBundle\Entity\Type $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \AppBundle\Entity\Type 
     */
    public function getType()
    {
        return $this->type;
    }
}
