<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * User
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\UserRepository")
 * @Serializer\ExclusionPolicy("all")
 */
class User implements AdvancedUserInterface
{
    /*
     * FOREIGN FIELDS
    */

    /**
     * @ORM\OneToMany(targetEntity="Allergy", mappedBy="user")
     */
    protected $allergy;
    /**
     * @ORM\OneToMany(targetEntity="Diet", mappedBy="user")
     */
    protected $diet;

    /*
     * FIELDS OF USER TABLE
    */


    /**
     * @var integer
     *
     * @ORM\Column(name="uid", type="string", length=36)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     *
     * @Serializer\Expose
     * @Serializer\Type("string")
     *
     * @Assert\Uuid()
     */
    private $uid;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=36)
     *
     * @Assert\Length(max=36)
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     */
    private $username;

    /**
     * @var integer
     *
     * @ORM\Column(name="password", type="string", length=8)
     *
     * @Serializer\Expose
     * @Serializer\Type("string")
     *
     * @Assert\Length(min=8, max=8)
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="json_array")
     */
    private $roles = array();

    /**
     * @ORM\Column(type="string", unique=true, nullable=true)
     */
    private $apiToken;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastLoginTime;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     *
     * * @Assert\Type(type="boolean")
     */
    private $isActive = true;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=32)
     *
     * @Serializer\Expose
     * @Serializer\Type("string")
     *
     * @Assert\NotNull()
     * @Assert\Length(min=3)
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="surname", type="string", length=32)
     *
     * @Serializer\Expose
     * @Serializer\Type("string")
     * @Serializer\Since("1.1")
     *
     * @Assert\NotNull()
     * @Assert\Length(min=3)
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     */
    private $surname;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     *
     * @Serializer\Expose
     * @Serializer\Type("string")
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Email
     */
    private $email;

    /**
     * @var integer
     *
     * @ORM\Column(name="n_meals", type="integer", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Type("integer")
     *
     * @Assert\Range(min = 1,max = 6)
     */
    private $n_meals;

    /**
     * @var integer
     *
     * @ORM\Column(name="protein", type="integer", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Type("integer")
     */
    private $protein;

    /**
     * @var integer
     *
     * @ORM\Column(name="carbohydrates", type="integer", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Type("integer")
     */
    private $carbohydrates;

    /**
     * @var integer
     *
     * @ORM\Column(name="fat", type="integer", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Type("integer")
     */
    private $fat;

    /**
     * @var float
     *
     * @ORM\Column(name="weight", type="float", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Type("float")
     */
    private $weight;

    /**
     * @var integer
     *
     * @ORM\Column(name="height", type="integer", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Type("integer")
     */
    private $height;

    /**
     * @var integer
     *
     * @ORM\Column(name="age", type="integer", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Type("integer")
     */
    private $age;

    /**
     * @var float
     *
     * @ORM\Column(name="wished_w", type="float", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Type("float")
     */
    private $wished_w;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->allergy = new \Doctrine\Common\Collections\ArrayCollection();
        $this->diet = new \Doctrine\Common\Collections\ArrayCollection();
        $this->apiToken = base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);
    }

    /**
     * Get uid
     *
     * @return string 
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set surname
     *
     * @param string $surname
     * @return User
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname
     *
     * @return string 
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set n_meals
     *
     * @param integer $nMeals
     * @return User
     */
    public function setNMeals($nMeals)
    {
        $this->n_meals = $nMeals;

        return $this;
    }

    /**
     * Get n_meals
     *
     * @return integer 
     */
    public function getNMeals()
    {
        return $this->n_meals;
    }

    /**
     * Set protein
     *
     * @param integer $protein
     * @return User
     */
    public function setProtein($protein)
    {
        $this->protein = $protein;

        return $this;
    }

    /**
     * Get protein
     *
     * @return integer 
     */
    public function getProtein()
    {
        return $this->protein;
    }

    /**
     * Set carbohydrates
     *
     * @param integer $carbohydrates
     * @return User
     */
    public function setCarbohydrates($carbohydrates)
    {
        $this->carbohydrates = $carbohydrates;

        return $this;
    }

    /**
     * Get carbohydrates
     *
     * @return integer 
     */
    public function getCarbohydrates()
    {
        return $this->carbohydrates;
    }

    /**
     * Set fat
     *
     * @param integer $fat
     * @return User
     */
    public function setFat($fat)
    {
        $this->fat = $fat;

        return $this;
    }

    /**
     * Get fat
     *
     * @return integer 
     */
    public function getFat()
    {
        return $this->fat;
    }

    /**
     * Set weight
     *
     * @param float $weight
     * @return User
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return float 
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set height
     *
     * @param integer $height
     * @return User
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get height
     *
     * @return integer 
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set age
     *
     * @param integer $age
     * @return User
     */
    public function setAge($age)
    {
        $this->age = $age;

        return $this;
    }

    /**
     * Get age
     *
     * @return integer 
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * Set wished_w
     *
     * @param float $wishedW
     * @return User
     */
    public function setWishedW($wishedW)
    {
        $this->wished_w = $wishedW;

        return $this;
    }

    /**
     * Get wished_w
     *
     * @return float 
     */
    public function getWishedW()
    {
        return $this->wished_w;
    }

    /**
     * Add allergy
     *
     * @param \AppBundle\Entity\Allergy $allergy
     * @return User
     */
    public function addAllergy(\AppBundle\Entity\Allergy $allergy)
    {
        $this->allergy[] = $allergy;

        return $this;
    }

    /**
     * Remove allergy
     *
     * @param \AppBundle\Entity\Allergy $allergy
     */
    public function removeAllergy(\AppBundle\Entity\Allergy $allergy)
    {
        $this->allergy->removeElement($allergy);
    }

    /**
     * Get allergy
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAllergy()
    {
        return $this->allergy;
    }

    /**
     * Add diet
     *
     * @param \AppBundle\Entity\Diet $diet
     * @return User
     */
    public function addDiet(\AppBundle\Entity\Diet $diet)
    {
        $this->diet[] = $diet;

        return $this;
    }

    /**
     * Remove diet
     *
     * @param \AppBundle\Entity\Diet $diet
     */
    public function removeDiet(Diet $diet)
    {
        $this->diet->removeElement($diet);
    }

    /**
     * Get diet
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDiet()
    {
        return $this->diet;
    }
    /*START NECESSARI PER LA USER INTERFACE*/
    public function getUsername()
    {
        return $this->username;
    }
    public function getPassword()
    {
        return $this->password;
    }
    public function getRoles()
    {
        $roles = $this->roles;
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }
    public function eraseCredentials()
    {
        // blank for now
    }
    public function getSalt()
    {
        return null;
    }
    /*END NECESSARI PER LA USER INTERFACE*/
    /*START NECESSARI PER LA ADVANCE USER INTERFACE*/
    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return true;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function isEnabled()
    {
        return $this->getIsActive();
    }
    /*END NECESSARI PER LA ADVANCE USER INTERFACE*/

    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Set roles
     *
     * @param array $roles
     * @return User
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set apiToken
     *
     * @param string $apiToken
     * @return User
     */
    public function setApiToken($apiToken)
    {
        $this->apiToken = $apiToken;

        return $this;
    }

    /**
     * Get apiToken
     *
     * @return string 
     */
    public function getApiToken()
    {
        return $this->apiToken;
    }

    /**
     * Set lastLoginTime
     *
     * @param \DateTime $lastLoginTime
     * @return User
     */
    public function setLastLoginTime($lastLoginTime)
    {
        $this->lastLoginTime = $lastLoginTime;

        return $this;
    }

    /**
     * Get lastLoginTime
     *
     * @return \DateTime 
     */
    public function getLastLoginTime()
    {
        return $this->lastLoginTime;
    }
}
