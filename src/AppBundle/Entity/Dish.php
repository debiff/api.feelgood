<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Dish
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\DishRepository")
 */
class Dish
{
    /*
     * FOREIGN FIELDS
    */

    /**
     * @ORM\ManyToOne(targetEntity="Food", inversedBy="dish_main")
     * @ORM\JoinColumn(name="main", referencedColumnName="fid")
     */
    protected $food_main;
    /**
     * @ORM\ManyToOne(targetEntity="Food", inversedBy="dish_outline1")
     * @ORM\JoinColumn(name="outline1", referencedColumnName="fid")
     */
    protected $food_outline1;
    /**
     * @ORM\ManyToOne(targetEntity="Food", inversedBy="dish_outline2")
     * @ORM\JoinColumn(name="outline2", referencedColumnName="fid")
     */
    protected $food_outline2;
    /**
     * @ORM\ManyToOne(targetEntity="Food", inversedBy="dish_outline3")
     * @ORM\JoinColumn(name="outline3", referencedColumnName="fid")
     */
    protected $food_outline3;
    /**
     * @ORM\ManyToOne(targetEntity="Food", inversedBy="dish_outline4")
     * @ORM\JoinColumn(name="outline4", referencedColumnName="fid")
     */
    protected $food_outline4;
    /**
     * @ORM\ManyToOne(targetEntity="Food", inversedBy="dish_outline5")
     * @ORM\JoinColumn(name="outline5", referencedColumnName="fid")
     */
    protected $food_outline5;
    /**
     * @ORM\ManyToOne(targetEntity="Food", inversedBy="dish_outline6")
     * @ORM\JoinColumn(name="outline6", referencedColumnName="fid")
     */
    protected $food_outline6;
    /**
     * @ORM\ManyToOne(targetEntity="Food", inversedBy="dish_outline7")
     * @ORM\JoinColumn(name="outline7", referencedColumnName="fid")
     */
    protected $food_outline7;
    /**
     * @ORM\ManyToOne(targetEntity="Food", inversedBy="dish_outline8")
     * @ORM\JoinColumn(name="outline8", referencedColumnName="fid", nullable=true)
     */
    protected $food_outline8;
    /**
     * @ORM\ManyToOne(targetEntity="Food", inversedBy="dish_outline9")
     * @ORM\JoinColumn(name="outline9", referencedColumnName="fid")
     */
    protected $food_outline9;
    /**
     * @ORM\OneToMany(targetEntity="Diet", mappedBy="dish")
     */
    protected $diet;

    /*
     * FIELDS OF ALLERGY TABLE
    */
    /**
     * @var integer
     *
     * @ORM\Column(name="did", type="string", length=36)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $did;

    /**
     * @var integer
     *
     * @ORM\Column(name="meal", type="integer")
     */
    private $meal;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->diet = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get did
     *
     * @return string 
     */
    public function getDid()
    {
        return $this->did;
    }

    /**
     * Set meal
     *
     * @param integer $meal
     * @return Dish
     */
    public function setMeal($meal)
    {
        $this->meal = $meal;

        return $this;
    }

    /**
     * Get meal
     *
     * @return integer 
     */
    public function getMeal()
    {
        return $this->meal;
    }

    /**
     * Set food_main
     *
     * @param \AppBundle\Entity\Food $foodMain
     * @return Dish
     */
    public function setFoodMain(\AppBundle\Entity\Food $foodMain = null)
    {
        $this->food_main = $foodMain;

        return $this;
    }

    /**
     * Get food_main
     *
     * @return \AppBundle\Entity\Food 
     */
    public function getFoodMain()
    {
        return $this->food_main;
    }

    /**
     * Set food_outline1
     *
     * @param \AppBundle\Entity\Food $foodOutline1
     * @return Dish
     */
    public function setFoodOutline1(\AppBundle\Entity\Food $foodOutline1 = null)
    {
        $this->food_outline1 = $foodOutline1;

        return $this;
    }

    /**
     * Get food_outline1
     *
     * @return \AppBundle\Entity\Food 
     */
    public function getFoodOutline1()
    {
        return $this->food_outline1;
    }

    /**
     * Set food_outline2
     *
     * @param \AppBundle\Entity\Food $foodOutline2
     * @return Dish
     */
    public function setFoodOutline2(\AppBundle\Entity\Food $foodOutline2 = null)
    {
        $this->food_outline2 = $foodOutline2;

        return $this;
    }

    /**
     * Get food_outline2
     *
     * @return \AppBundle\Entity\Food 
     */
    public function getFoodOutline2()
    {
        return $this->food_outline2;
    }

    /**
     * Set food_outline3
     *
     * @param \AppBundle\Entity\Food $foodOutline3
     * @return Dish
     */
    public function setFoodOutline3(\AppBundle\Entity\Food $foodOutline3 = null)
    {
        $this->food_outline3 = $foodOutline3;

        return $this;
    }

    /**
     * Get food_outline3
     *
     * @return \AppBundle\Entity\Food 
     */
    public function getFoodOutline3()
    {
        return $this->food_outline3;
    }

    /**
     * Set food_outline4
     *
     * @param \AppBundle\Entity\Food $foodOutline4
     * @return Dish
     */
    public function setFoodOutline4(\AppBundle\Entity\Food $foodOutline4 = null)
    {
        $this->food_outline4 = $foodOutline4;

        return $this;
    }

    /**
     * Get food_outline4
     *
     * @return \AppBundle\Entity\Food 
     */
    public function getFoodOutline4()
    {
        return $this->food_outline4;
    }

    /**
     * Set food_outline5
     *
     * @param \AppBundle\Entity\Food $foodOutline5
     * @return Dish
     */
    public function setFoodOutline5(\AppBundle\Entity\Food $foodOutline5 = null)
    {
        $this->food_outline5 = $foodOutline5;

        return $this;
    }

    /**
     * Get food_outline5
     *
     * @return \AppBundle\Entity\Food 
     */
    public function getFoodOutline5()
    {
        return $this->food_outline5;
    }

    /**
     * Set food_outline6
     *
     * @param \AppBundle\Entity\Food $foodOutline6
     * @return Dish
     */
    public function setFoodOutline6(\AppBundle\Entity\Food $foodOutline6 = null)
    {
        $this->food_outline6 = $foodOutline6;

        return $this;
    }

    /**
     * Get food_outline6
     *
     * @return \AppBundle\Entity\Food 
     */
    public function getFoodOutline6()
    {
        return $this->food_outline6;
    }

    /**
     * Set food_outline7
     *
     * @param \AppBundle\Entity\Food $foodOutline7
     * @return Dish
     */
    public function setFoodOutline7(\AppBundle\Entity\Food $foodOutline7 = null)
    {
        $this->food_outline7 = $foodOutline7;

        return $this;
    }

    /**
     * Get food_outline7
     *
     * @return \AppBundle\Entity\Food 
     */
    public function getFoodOutline7()
    {
        return $this->food_outline7;
    }

    /**
     * Set food_outline8
     *
     * @param \AppBundle\Entity\Food $foodOutline8
     * @return Dish
     */
    public function setFoodOutline8(\AppBundle\Entity\Food $foodOutline8 = null)
    {
        $this->food_outline8 = $foodOutline8;

        return $this;
    }

    /**
     * Get food_outline8
     *
     * @return \AppBundle\Entity\Food 
     */
    public function getFoodOutline8()
    {
        return $this->food_outline8;
    }

    /**
     * Set food_outline9
     *
     * @param \AppBundle\Entity\Food $foodOutline9
     * @return Dish
     */
    public function setFoodOutline9(\AppBundle\Entity\Food $foodOutline9 = null)
    {
        $this->food_outline9 = $foodOutline9;

        return $this;
    }

    /**
     * Get food_outline9
     *
     * @return \AppBundle\Entity\Food 
     */
    public function getFoodOutline9()
    {
        return $this->food_outline9;
    }

    /**
     * Add diet
     *
     * @param \AppBundle\Entity\Diet $diet
     * @return Dish
     */
    public function addDiet(\AppBundle\Entity\Diet $diet)
    {
        $this->diet[] = $diet;

        return $this;
    }

    /**
     * Remove diet
     *
     * @param \AppBundle\Entity\Diet $diet
     */
    public function removeDiet(\AppBundle\Entity\Diet $diet)
    {
        $this->diet->removeElement($diet);
    }

    /**
     * Get diet
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDiet()
    {
        return $this->diet;
    }
}
