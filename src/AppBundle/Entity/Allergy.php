<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Allergy
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\AllergyRepository")
 */
class Allergy
{
    /*
     * FOREIGN FIELDS
    */

    /**
     * @ORM\ManyToOne(targetEntity="Food", inversedBy="allergy")
     * @ORM\JoinColumn(name="fid", referencedColumnName="fid")
     */
    protected $food;
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="allergy")
     * @ORM\JoinColumn(name="uid", referencedColumnName="uid")
     */
    protected $user;

    /*
     * FIELDS OF ALLERGY TABLE
    */
    /**
     * @var integer
     *
     * @ORM\Column(name="aid", type="string", length=36)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $aid;



    /**
     * Get aid
     *
     * @return string 
     */
    public function getAid()
    {
        return $this->aid;
    }

    /**
     * Set food
     *
     * @param \AppBundle\Entity\Food $food
     * @return Allergy
     */
    public function setFood(\AppBundle\Entity\Food $food = null)
    {
        $this->food = $food;

        return $this;
    }

    /**
     * Get food
     *
     * @return \AppBundle\Entity\Food 
     */
    public function getFood()
    {
        return $this->food;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     * @return Allergy
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
