<?php
/**
 * Created by PhpStorm.
 * User: simone
 * Date: 15/07/15
 * Time: 16.40
 */

namespace AppBundle\Controller;
use AppBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations\Prefix;
use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\Controller\Annotations\Post;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Class UserController
 * @package AppBundle\Controller
 * @Prefix("")
 * @NamePrefix("api_v1_users_")
 */
class UserController extends ApiController{
    /**
     * @ApiDoc(
     *  description="Create a new Object",
     *  output = {
     *     "class" = "AppBundle\Entity\User",
     *     "parsers" = {
     *       "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *     }
     *   },
     *     statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized to say hello",
     *         404={
     *           "Returned when the user is not found",
     *           "Returned when something else is not found"
     *         }
     *     }
     * )
     * @return Response
     */
    public function getAction(){
        try{
            $em = $this->getDoctrine()->getManager();
            $users = $em->getRepository("AppBundle:User")->findAll();
            $response = $this->serialize($users, 'json');
            return new Response($response,200);
        }catch (\Exception $e){
            $error[]=array(array('message'=>$e->getMessage()));
            return new Response($this->serialize($error),500);
        }

    }

    /**
     * @ApiDoc(
     *  description="Create a new Object",
     *  output = {
     *     "class" = "AppBundle\Entity\User",
     *     "parsers" = {
     *       "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *     }
     *   },
     *     statusCodes={
     *         201="Returned when successful",
     *         403="Returned when the user is not authorized to say hello",
     *         404={
     *           "Returned when the user is not found",
     *           "Returned when something else is not found"
     *         }
     *     }
     * )
     * @param Request $request
     * @return Response
     * @throws HttpException
     */
    public function postAction(Request $request){
        $user=$this->deserialize($request,'AppBundle\Entity\User');
        $errors=$this->validation($user);
        if (count($errors )>0) {
            return new Response($this->serialize($errors),400);
        }else{
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            return new Response($this->serialize($user), 201);
        }
    }

    /**
     * @param Request $request
     * @return Response
     * @Post("/login")
     */
    public function loginAction(Request $request){
        $credential=json_decode($request->getContent(),true);
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')->getKey($credential['username'],$credential['password']);
        if($user!=null){
            return new Response($this->serialize($user), 200);
        }else{
            $response=array();
            $response['message']='Utente non trovato';
            return new Response($this->serialize($response), 401);
        }

    }




}